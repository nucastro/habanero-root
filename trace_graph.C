#include <vector>
#include <string>
#include <cstddef>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>

// Graph the traces
void trace_graph(std::string fname = "trace.out", Int_t slotid = 2, Int_t chid = 4) {
  FILE *ftr = fopen(fname.c_str(), "rb");
  if (ftr == NULL) {
    fprintf(stderr, "Error opening \'%s\'\n", fname.c_str());
    return;
  }

  TMultiGraph *mg = new TMultiGraph("mg", "Traces");

  std::vector<int> xs;
  std::vector<int> ys;

  Int_t nTraces;
  fread(&nTraces, 4, 1, ftr);

  // Read in all traces (or set nTraces to whatever you want the max to be)
  for (Int_t trNum = 0; !feof(ftr) && trNum < nTraces; ++trNum) {
    printf("%5.2f%%   \r", 100.*trNum/nTraces);

    xs.clear();
    ys.clear();
    unsigned short int so, cr, sl, ch, s, en;
    unsigned short int buf;

    fread(&so, 2, 1, ftr);
    fread(&cr, 2, 1, ftr);
    fread(&sl, 2, 1, ftr);
    fread(&ch, 2, 1, ftr);
    fread(&en, 2, 1, ftr);
    fread(&s, 2, 1, ftr);

    // Read in one 16-bit word at a time
    for (size_t i = 0; i < s; ++i) {
      fread(&buf, 2, 1, ftr);

      // There are 10 ns between each point
      xs.push_back(10*i);
      ys.push_back(buf);
    }

    // Only showing one channel at a time
    if (sl == slotid && ch == chid) {
      TGraph* g = new TGraph(s, &xs[0], &ys[0]);
      mg->Add(g);
    }
  }
  printf("\n");

  // These options are very important (must have at least AL or AC)
  // A draws the axes
  // L draws a straught line between points
  // C draws a curve that goes through the points
  mg->Draw("AL");
}
